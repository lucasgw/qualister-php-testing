<?php

class ApiTest extends PHPUnit_Framework_TestCase {

	private $client;

	protected function setUp() {
		$this->client = new GuzzleHttp\Client(["base_uri" => "http://localhost:8080/"]);
	}

	public function testAcessarRootEntaoReceber200SucessoECodigo() {

		$resposta = $this->client->get(
			"",
			["auth" => ["phptesting", "123"]]
			);

		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals("application/json", $resposta->getHeader("content-type")[0]);
		
		$body = json_decode($resposta->getBody(), true);
		
		$this->assertEquals("sucesso", $body["status"]);
		$this->assertEquals("Bem vindo à API do Bar do Lucas", $body["message"]);
		$this->assertEquals("gnitsetphp", $body["data"]["codigo"]);
	}

	public function testChamarPedidoEReceber200SucessoListaVazia() {

		$resposta = $this->client->get(
			"pedido",
			['auth' => ['phptesting', '123']]
			);

		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals("application/json", $resposta->getHeader('content-type')[0]);

		$body = json_decode($resposta->getBody(), true);
		
		$this->assertEquals("successo", $body["status"]);
		$this->assertEquals("A lista está vazia", $body["message"]);
		$this->assertCount(0, $body["data"]);
	}

	public function testChamarPedido1EReceber200SucessoNomeECodigoCliente() {

		$resposta = $this->client->get(
			"pedido/69?clientenome=Lucas",
			['auth' => ['phptesting', '123']
			]);
		
		$this->assertEquals(200, $resposta->getStatusCode());
		$this->assertEquals("application/json", $resposta->getHeader("content-type")[0]);
		
		$body = json_decode($resposta->getBody(), true);
		
		$this->assertEquals("successo", $body["status"]);
		$this->assertEquals("Seu código é 69", $body["message"]);
		$this->assertEquals("Lucas", $body["data"]["clientenome"]);
	}

	/**
	*	@dataProvider dadosTeste
	*/
	public function testAdicionarItemAoPedidoERetornarSucesso( $id, $nome, $estoque, $valor, $quantidade, $user, $pwd, $status ) {

		$resposta = $this->client->post(
			"pedido", [
			"auth" => [$user, $pwd],
			"form_params" => [
			"produtoid" => $id,
			"produtonome" => $nome,
			"produtoestoque" => $estoque,
			"produtovalor" => $valor,
			"quantidade" => $quantidade]
			]
			);

		$body = json_decode($resposta->getBody(), true);

		$this->assertEquals($status, $body["message"]);
	}

	public function dadosTeste(){
		return array_map(
			'str_getcsv',
			file('tests/integration/ddt/testAdicionarItemAoPedidoERetornarSucesso.csv')
			);
	}

	/**
	* @group checkSchema
	*/
	public function testValidarUmSchemaJson() {

		$resposta = $this->client->get(
			"",
			['auth' => ['phptesting', '123']]
			);

		$validator = new \Json\Validator('tests/integration/schema/schema.json');

		$validator->validate(json_decode($resposta->getBody()));
	}
}

?>