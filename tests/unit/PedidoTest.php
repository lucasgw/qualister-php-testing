<?php

class PedidoTest extends PHPUnit_Framework_TestCase {
	
	public static function setUpBeforeClass() {
		echo "Iniciando os Testes\n";
	}

	private $pedido;
	
	/**
	* @covers Pedido
	*/
	protected function setUp() {
		$this->pedido = new Pedido();
	}
	
	/**
	 * @test
	 * @group Importantes
	 * @author Teles
	 * @small
	 * @covers Pedido
	 * @covers Pedido::getPedidoItens
	 */
	public function listaDePedidosDeveConter0Itens() {		
		
		$pedidoItens = $this->pedido->getPedidoItens();		
		$this->assertCount(0, $pedidoItens);
		
	}
	
	/**
	 * @test
	 * @group Importantes
	 * @author Teles
	 * @medium
	 * @covers Produto
	 * @covers Pedido
	 * @covers Pedido::addItemPedido
	 * @covers Pedido::getPedidoItens
	 * @dataProvider dataAoAdicionar2ProdutosESalvarOStatusSeraSucesso
	 */
	public function aoAdicionarProdutoAListaDeveraConterApenas1Item( $id1 , $nome1 , $est1 , $val1 , $qtd1 ) {
		// Arrange
		$produto = $this->getMockBuilder("ProdutoInterface")->setConstructorArgs(array($id1 , $nome1 , $est1 , $val1))->getMock();
		// Act
		$this->pedido->addItemPedido($produto, $qtd1);
		$pedidoItens = $this->pedido->getPedidoItens();
		// Assert
		$this->assertCount(1, $pedidoItens);
		$this->assertEquals(array($produto, $qtd1), $pedidoItens[0]);
	}
	
	/**
	 * @group excecoes
	 * @expectedException Exception
	 * @expectedExceptionCode 20
	 * @expectedExceptionMessage Problema
	 */
	public function testLancarExceptionEoTipoSeraException() {
		throw new Exception("Problema", 20);
	}

	/**
	 * @test
	 * @author Teles
	 * @medium
	 * @covers Produto
	 * @covers Pedido
	 * @covers Pedido::addItemPedido
	 * @covers Pedido::getPedidoItens
	 * @dataProvider dataAoAdicionar2ProdutosESalvarOStatusSeraSucesso
	 */
	public function aoAdicionar2ProdutosESalvarOStatusSeraSucesso( $id1 , $nome1 , $est1 , $val1 , $qtd1 , $id2 , $nome2 , $est2 , $val2 , $qtd2 , $resp ) {
		
		$produto1 = $this->getMockBuilder("ProdutoInterface")->setConstructorArgs(array($id1 , $nome1 , $est1 , $val1))->getMock();
		$produto2 = $this->getMockBuilder("ProdutoInterface")->setConstructorArgs(array($id2 , $nome2 , $est2 , $val2))->getMock();
		
		$pedidoServicos = $this->getMockBuilder("PedidoServicosInterface")->setMethods(array("salvar"))->getMock();
		$pedidoServicos->expects($this->once())->method("salvar")->with($this->isInstanceOf("PedidoInterface"))->willReturn("Sucesso");

		$this->pedido->addItemPedido($produto1, $qtd1);
		$this->pedido->addItemPedido($produto2, $qtd2);
		
		$this->assertEquals($resp, $pedidoServicos->salvar($this->pedido));
	}

	function dataAoAdicionar2ProdutosESalvarOStatusSeraSucesso() {

		return array_map(
				"str_getcsv",
				file("tests/unit/ddt/dataAoAdicionar2ProdutosESalvarOStatusSeraSucesso.csv")
			);
	}
	
	public static function tearDownAfterClass() {
		echo "Finalizando os Testes\n";
	}
}

?>