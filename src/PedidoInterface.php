<?php 

interface PedidoInterface {

	public function getPedidoItens();
	public function addItemPedido(ProdutoInterface $produto, $quantidade);
}

?>