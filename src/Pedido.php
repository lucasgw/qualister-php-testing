<?php

class Pedido implements PedidoInterface {
	private $pedidoItens = array();

	public function getPedidoItens()
	{
		return $this->pedidoItens;
	}

	public function addItemPedido(ProdutoInterface $produto, $quantidade)
	{
		$this->pedidoItens[] = array($produto, $quantidade);
	}

}

?>